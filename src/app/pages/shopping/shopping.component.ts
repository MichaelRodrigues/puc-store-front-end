import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { environment } from '../../../environments/environment';
import {FormProductData} from '../../shared/models/FormProductData';
import {SaleComplete} from '../../shared/models/SaleComplete';
import {LoadingService} from '../../shared/services/loading.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.css']
})
export class ShoppingComponent implements OnInit {

  sales: SaleComplete[];

  constructor(private authGuardService: AuthGuardService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private toastr: ToastrService) {
      this.sales = [];
  }

  ngOnInit() {
    this.loadingService.showSreenLoading();

    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': this.authGuardService.getLoggedUser().token
      })
    };

    this.httpClient.get(environment.baseUrl + '/shopping', httpOptions)
    .subscribe(
        data => {
          this.sales = data['result'];
          this.loadingService.hideSreenLoading();
        },
        error => {
          console.log(error);
          this.loadingService.hideSreenLoading();
        }
    );
  }

  calcValue(products: FormProductData[]): number {
    return products.reduce((a, b) => +a + +b.price, 0);
  }

  calcDate(milliseconds: number) {
    const date = new Date(milliseconds);

    return this.pad(date.getDate(), 2) + '/' + this.pad(date.getMonth(), 2) + '/' + date.getFullYear();
  }

  pad(num, size) {
    let s = num + '';
    while (s.length < size) { s = '0' + s; }
    return s;
  }


}

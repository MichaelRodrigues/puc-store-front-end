import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { environment } from '../../../environments/environment';
import {FormProductData} from '../../shared/models/FormProductData';
import {LoadingService} from '../../shared/services/loading.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {

  products: FormProductData[];

  constructor(private authGuardService: AuthGuardService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private router: Router,
    private toastr: ToastrService) {
      this.products = [];

      if (!authGuardService.userIsLogged()) {
        this.router.navigate(['/login'], {});
      }
  }

  ngOnInit() {
    this.loadingService.showSreenLoading();

    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': this.authGuardService.getLoggedUser().token
      })
    };

    this.httpClient.get(environment.baseUrl + '/sales', httpOptions)
    .subscribe(
        data => {
          this.products = data['result'];
          this.loadingService.hideSreenLoading();
        },
        error => {
          console.log(error);
          this.loadingService.hideSreenLoading();
        }
    );
  }

}

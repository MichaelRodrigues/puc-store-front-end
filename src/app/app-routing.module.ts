import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppPageHomeComponent } from './pages/home/page.home.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { AccountComponent } from './pages/account/account.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductNewComponent } from './pages/product-new/product-new.component';
import { CartComponent } from './pages/cart/cart.component';
import { ShoppingComponent } from './pages/shopping/shopping.component';
import { SalesComponent } from './pages/sales/sales.component';
import { SearchComponent } from './pages/search/search.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
  { path: 'home', component: AppPageHomeComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'account', component: AccountComponent },
  { path: 'shopping', component: ShoppingComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'product-new', component: ProductNewComponent },
  { path: 'sales', component: SalesComponent },
  { path: 'cart', component: CartComponent },
  { path: 'search', component: SearchComponent },
  { path: 'users', component: UsersComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})

export class AppRoutingModule {}

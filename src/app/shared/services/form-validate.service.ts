import { Injectable } from '@angular/core';
import {FormRegisterData} from '../../shared/models/FormRegisterData';
import {FormLoginData} from '../../shared/models/FormLoginData';
import {FormProductData} from '../../shared/models/FormProductData';
import {FormValidationResponse} from '../../shared/models/FormValidationResponse';

@Injectable({
  providedIn: 'root'
})
export class FormValidateService {

  constructor() { }

  validateSigUp(formData: FormRegisterData): FormValidationResponse {
    if (!formData.name || formData.name.trim().length < 3) {
      return new FormValidationResponse({success: false, field: 'name'});
    } else {
      if (!formData.email || formData.email.trim().length < 3) {
        return new FormValidationResponse({success: false, field: 'email'});
      } else {
        if (!formData.password ||  formData.password.trim().length < 3) {
          return new FormValidationResponse({success: false, field: 'password'});
        } else {
          return new FormValidationResponse({success: true});
        }
      }
    }
  }

  validateLogin(formData: FormLoginData): FormValidationResponse {
    if (!formData.identifier || formData.identifier.trim().length < 3) {
      return new FormValidationResponse({success: false, field: 'email'});
    } else {
      if (!formData.password ||  formData.password.trim().length < 3) {
        return new FormValidationResponse({success: false, field: 'password'});
      } else {
        return new FormValidationResponse({success: true});
      }
    }
  }

  validateNewProduct(formData: FormProductData): FormValidationResponse {
    if (!formData.name || formData.name.trim().length < 3) {
      return new FormValidationResponse({success: false, field: 'name'});
    } else {
      if (!formData.price) {
        return new FormValidationResponse({success: false, field: 'price'});
      } else {
        if (!formData.description ||  formData.description.trim().length < 3) {
          return new FormValidationResponse({success: false, field: 'description'});
        } else {
          if (!formData.departament ||  formData.departament.trim().length < 3) {
            return new FormValidationResponse({success: false, field: 'departament'});
          } else {
            return new FormValidationResponse({success: true});
          }
        }
      }
    }
  }
}

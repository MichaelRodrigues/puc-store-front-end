export class FormValidationResponse {
    success: boolean;
    message: string;
    field: string;

    public constructor(init?: Partial<FormValidationResponse>) {
        Object.assign(this, init);
    }
}

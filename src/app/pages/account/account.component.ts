import { Component, OnInit } from '@angular/core';
import {User} from '../../shared/models/User';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  userData: User = new User();

  constructor(
    private authGuardService: AuthGuardService,
    private router: Router) {

    if (!authGuardService.userIsLogged()) {
      this.router.navigate(['/login'], {});
    }
  }

  ngOnInit() {
    this.userData = this.authGuardService.getLoggedUser();
  }

}

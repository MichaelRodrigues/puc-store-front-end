export class FormProductData {
  id: string;
  name: string;
  price: string;
  description: string;
  departament: string;
  pictures: string[];
  enabled: boolean;
  user: string;

  public constructor(init?: Partial<FormProductData>) {
    Object.assign(this, init);
  }
}

/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Set Header
3. Init Menu
4. Init Timer
5. Init Favorite
6. Init Fix Product Border
7. Init Isotope Filtering
8. Init Slider


******************************/

jQuery(document).ready(function($)
{
	"use strict";

	var header = $('.header');
	var menu = $('.hamburger_menu');
	var fsOverlay = $('.fs_menu_overlay');
  

	setHeader();

	$(window).on('resize', function()
	{
		initFixProductBorder();
		setHeader();
	});

	$(document).on('scroll', function()
	{
		setHeader();
	});

	initFavorite();

	function initFixProductBorder(){
        if($('.product_filter').length){
			var products = $('.product_filter:visible');
			var wdth = window.innerWidth;

			// reset border
			products.each(function()
			{
				$(this).css('border-right', 'solid 1px #e9e9e9');
			});

			// if window width is 991px or less

			if(wdth < 480)
			{
			for(var i = 0; i < products.length; i++)
			{
				var product = $(products[i]);
				product.css('border-right', 'none');
			}
			}

			else if(wdth < 576)
			{
			if(products.length < 5)
			{
				var product = $(products[products.length - 1]);
				product.css('border-right', 'none');
			}
			for(var i = 1; i < products.length; i+=2)
			{
				var product = $(products[i]);
				product.css('border-right', 'none');
			}
			}

			else if(wdth < 768)
			{
			if(products.length < 5)
			{
				var product = $(products[products.length - 1]);
				product.css('border-right', 'none');
			}
			for(var i = 2; i < products.length; i+=3)
			{
				var product = $(products[i]);
				product.css('border-right', 'none');
			}
			}

			else if(wdth < 992)
			{
			if(products.length < 5)
			{
				var product = $(products[products.length - 1]);
				product.css('border-right', 'none');
			}
			for(var i = 3; i < products.length; i+=4)
			{
				var product = $(products[i]);
				product.css('border-right', 'none');
			}
			}

			//if window width is larger than 991px
			else
			{
			if(products.length < 5)
			{
				var product = $(products[products.length - 1]);
				product.css('border-right', 'none');
			}
			for(var i = 4; i < products.length; i+=5)
			{
				var product = $(products[i]);
				product.css('border-right', 'none');
			}
			}	
    	}
    }

	function setHeader()
	{
		if(window.innerWidth < 992)
		{
			if($(window).scrollTop() > 100)
			{
				header.css({'top':"0"});
			}
			else
			{
				header.css({'top':"0"});
			}
		}
		else
		{
			if($(window).scrollTop() > 100)
			{
				header.css({'top':"-50px"});
			}
			else
			{
				header.css({'top':"0"});
			}
		}
		if(window.innerWidth > 991)
		{
			closeMenu();
		}
	}

    function initFavorite()
    {
    	if($('.favorite').length)
    	{
    		var favs = $('.favorite');

    		favs.each(function()
    		{
    			var fav = $(this);
    			var active = false;
    			if(fav.hasClass('active'))
    			{
    				active = true;
    			}

    			fav.on('click', function()
    			{
    				if(active)
    				{
    					fav.removeClass('active');
    					active = false;
    				}
    				else
    				{
    					fav.addClass('active');
    					active = true;
    				}
    			});
    		});
    	}
	}
    
    function closeMenu(){
        menu.removeClass('active');
        fsOverlay.css('pointer-events', "none");
    }
});
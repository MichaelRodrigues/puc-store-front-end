import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { environment } from '../../../environments/environment';
import {FormProductData} from '../../shared/models/FormProductData';
import {LoadingService} from '../../shared/services/loading.service';
import { ToastrService } from 'ngx-toastr';
import {CartService} from '../../shared/services/cart.service';
import { ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {

  arrivals: FormProductData[];
  searchvalue: string;
  env = environment;

  constructor(private authGuardService: AuthGuardService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private toastr: ToastrService,
    private cartService: CartService,
    private route: ActivatedRoute) {
      this.arrivals = [];
      this.route.queryParams.subscribe(params => {
        this.searchvalue = params['value'];
      });
    }

  ngOnInit() {

    this.loadProducts(this.searchvalue);

    this.route
      .queryParams
      .subscribe(queryParams => {
        this.loadProducts(queryParams['value']);
      });
  }


  loadProducts(value: string) {
    if (this.authGuardService.getLoggedUser()) {
      this.loadingService.showSreenLoading();

      const httpOptions = {
        headers: new HttpHeaders({
          'X-Auth-Token': this.authGuardService.getLoggedUser().token
        })
      };

      this.httpClient.get(environment.baseUrl + '/product/search/' + value, httpOptions)
      .subscribe(
          data => {
            this.arrivals = data['result'];
            this.loadingService.hideSreenLoading();
          },
          error => {
            console.log(error);
            this.loadingService.hideSreenLoading();
          }
      );
    }
  }

  addToCart(product: FormProductData) {
    this.cartService.addToCart(product);
    this.toastr.success('Produto adicionado ao carrinho.', 'Parabéns', {
      timeOut: 500
    });
  }

}

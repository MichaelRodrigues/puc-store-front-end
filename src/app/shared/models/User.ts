export class User {
  identifier: string;
  email: string;
  token: string;
  name: string;
  torkeneExpiresOn: string;

  public constructor(init?: Partial<User>) {
    Object.assign(this, init);
  }
}

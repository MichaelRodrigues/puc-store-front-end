import {FormProductData} from '../../shared/models/FormProductData';

export class Sale {
  producs: FormProductData[];

  public constructor(init?: Partial<Sale>) {
    Object.assign(this, init);
  }
}

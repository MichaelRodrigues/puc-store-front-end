import {FormProductData} from '../../shared/models/FormProductData';
import {User} from '../../shared/models/User';

export class SaleComplete {
  id: string;
  user: User;
  moment: number;
  products: FormProductData[];
  paid: boolean;
  delivered: boolean;

  public constructor(init?: Partial<SaleComplete>) {
    Object.assign(this, init);
  }
}


import { Component, OnInit, EventEmitter, Inject} from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { FileUploaderCustom } from './FileUploaderCustom';
import {FormProductData} from '../../shared/models/FormProductData';
import {FormValidateService} from '../../shared/services/form-validate.service';
import {LoadingService} from '../../shared/services/loading.service';
import { DOCUMENT } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-new',
  templateUrl: './product-new.component.html',
  styleUrls: ['./product-new.component.css']
})
export class ProductNewComponent implements OnInit {

  uploader: FileUploaderCustom;
  hasBaseDropZoneOver: boolean;
  hasAnotherDropZoneOver: boolean;
  response: string;
  formData: FormProductData;

  constructor(private httpClient: HttpClient,
    private formValidateService: FormValidateService,
    @Inject(DOCUMENT) document,
    private toastr: ToastrService,
    private router: Router,
    private loadingService: LoadingService,
    private authGuardService: AuthGuardService) {
    this.formData = new FormProductData();

    this.formData.departament = 'car';
    this.formData.pictures = [];

    if (!authGuardService.userIsLogged()) {
      this.router.navigate(['/login'], {});
    }
  }

  ngOnInit() {
  }

  fileChange(event): void {
    this.loadingService.showSreenLoading();

    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
        const file = fileList[0];

        const formData = new FormData();
        formData.append('file', file, file.name);

        this.httpClient.post(environment.baseUrl + '/upload/product/image', formData, {
          headers: new HttpHeaders().set('X-Auth-Token', this.authGuardService.getLoggedUser().token)
        })
        .subscribe(
          data => {
            this.formData.pictures.push(data['picture']);
            this.loadingService.hideSreenLoading();

          },
          error => {
            this.loadingService.hideSreenLoading();
          console.log(error);
          }
        );
    }
  }

  sendForm() {
    this.loadingService.showSreenLoading();
    const inputs = document.getElementsByClassName('form_input');

    Array.prototype.forEach.call(inputs, function(el) {
      el.style.borderColor = '#e5e5e5';
    });

    const validation = this.formValidateService.validateNewProduct(this.formData);

    if (validation.success) {
      this.formData.enabled = false;
      this.formData.user = this.authGuardService.getLoggedUser().email;

      this.httpClient.post(environment.baseUrl + '/product', this.formData, {
        headers: new HttpHeaders().set('X-Auth-Token', this.authGuardService.getLoggedUser().token)
      })
      .subscribe(
          data => {
              this.formData = new FormProductData();
              this.formData.departament = 'car';
              this.formData.pictures = [];

              this.loadingService.hideSreenLoading();

              this.toastr.success('Produto cadastrado com sucesso.', 'Parabéns', {
                timeOut: 1500
              });

              const pubRouter = this.router;
              setTimeout(function() {
                pubRouter.navigate(['/products'], {});
              }, 1500);
          },
          error => {
            this.toastr.error('Os dados enviados contém erros.', 'Ocorreu um erro', {
              timeOut: 3000
            });

            this.loadingService.hideSreenLoading();
          }
      );
    } else {
      this.loadingService.hideSreenLoading();

      this.toastr.error('Por gentileza, preencha o formulário corretamente.', 'Preenchimento Errado', {
        timeOut: 3000
      });

      const input = document.getElementById('input_' + validation.field);
      input.style.borderColor = '#f00';
    }
  }

}

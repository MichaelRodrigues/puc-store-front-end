import { Injectable } from '@angular/core';
import {User} from '../../shared/models/User';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router) { }

  userIsLogged(): boolean {
    if (localStorage.getItem('currentUser')) {
      return true;
    }
    return false;
  }

  setLoggedUser(user: User ) {
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  getLoggedUser(): User {
    const user = localStorage.getItem('currentUser');
    if (user) {
      return new User(JSON.parse(user));
    }
    return null;
  }

  logout() {
    localStorage.removeItem('currentUser');

    const pubRouter = this.router;
    setTimeout(function() {
      pubRouter.navigate(['/'], {});
    }, 1500);

  }

}

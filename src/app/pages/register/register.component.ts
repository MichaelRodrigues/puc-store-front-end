import { Component, OnInit, Inject} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormRegisterData} from '../../shared/models/FormRegisterData';
import {FormValidateService} from '../../shared/services/form-validate.service';
import {LoadingService} from '../../shared/services/loading.service';
import { DOCUMENT } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { ifError } from 'assert';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  formData: FormRegisterData;

  constructor(private httpClient: HttpClient,
    private formValidateService: FormValidateService,
    @Inject(DOCUMENT) document,
    private toastr: ToastrService,
    private router: Router,
    private loadingService: LoadingService) {
    this.formData = new FormRegisterData();
  }

  ngOnInit() {

  }

  sendForm(profile: string) {
    this.loadingService.showSreenLoading();
    const inputs = document.getElementsByClassName('form_input');

    Array.prototype.forEach.call(inputs, function(el) {
      el.style.borderColor = '#e5e5e5';
    });

    const validation = this.formValidateService.validateSigUp(this.formData);

    if (validation.success) {
      this.formData.identifier = this.formData.email;
      this.formData.profile = profile;

      this.httpClient.post(environment.baseUrl + '/auth/signup', this.formData)
      .subscribe(
          data => {
              this.formData = {name: '', identifier: '', profile: '', email: '', password: ''};
              this.loadingService.hideSreenLoading();

              this.toastr.success('Cadastro realizado com sucesso.', 'Parabéns', {
                timeOut: 1500
              });

              const pubRouter = this.router;
              setTimeout(function() {
                pubRouter.navigate(['/login'], {});
              }, 1500);
          },
          error => {
            this.toastr.error(error.error.error, 'Ocorreu um erro', {
              timeOut: 3000
            });

            this.loadingService.hideSreenLoading();
          }
      );
    } else {
      this.loadingService.hideSreenLoading();

      this.toastr.error('Por gentileza, preencha o formulário corretamente.', 'Preenchimento Errado', {
        timeOut: 3000
      });

      const input = document.getElementById('input_' + validation.field);
      input.style.borderColor = '#f00';
    }
  }

}

import { Component, OnInit } from '@angular/core';
import {AuthGuardService} from '../../../shared/services/auth-guard.service';
import {User} from '../../../shared/models/User';
import {CartService} from '../../../shared/services/cart.service';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import {FormProductData} from '../../../shared/models/FormProductData';
import {SaleComplete} from '../../../shared/models/SaleComplete';
import {LoadingService} from '../../../shared/services/loading.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  currentUser: User;
  searchvalue: string;

  constructor(private authGuardService: AuthGuardService,
    private cartService: CartService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router) {
    this.currentUser = authGuardService.getLoggedUser();
    this.searchvalue = '';
  }

  ngOnInit() {
    jQuery(document).ready(function($) {
      const hamburger = $('.hamburger_container');
      const menu = $('.hamburger_menu');
      let menuActive = false;
      const hamburgerClose = $('.hamburger_close');
      const fsOverlay = $('.fs_menu_overlay');

      if (hamburger.length) {
        hamburger.on('click', function() {
          if (!menuActive) {
            openMenu();
          }
        });
      }
      if (fsOverlay.length) {
        fsOverlay.on('click', function() {
          if (menuActive) {
            closeMenu();
          }
        });
      }
      if (hamburgerClose.length) {
        hamburgerClose.on('click', function() {
          if (menuActive) {
            closeMenu();
          }
        });
      }

      if ($('.menu_item').length) {
        const items = document.getElementsByClassName('menu_item');
        let i;
        for (i = 0; i < items.length; i++) {
          if (items[i].classList.contains('has-children')) {
            $(items[i]).onclick = function() {
              this.classList.toggle('active');
              const panel = this.children[1];
              if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
              } else {
                panel.style.maxHeight = panel.scrollHeight + 'px';
              }
            };
          }
        }
      }
      function openMenu() {
        menu.addClass('active');
        // menu.css('right', "0");
        fsOverlay.css('pointer-events', 'auto');
        menuActive = true;
      }
      function closeMenu() {
        menu.removeClass('active');
        fsOverlay.css('pointer-events', 'none');
        menuActive = false;
      }

    });
  }

  logout() {
    this.authGuardService.logout();
  }

  searchKeyEvent(event) {
    this.router.navigate(['/search'], { queryParams: { value: this.searchvalue } });
  }

}

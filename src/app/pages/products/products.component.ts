import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { environment } from '../../../environments/environment';
import {FormProductData} from '../../shared/models/FormProductData';
import {LoadingService} from '../../shared/services/loading.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: FormProductData[];

  constructor(private authGuardService: AuthGuardService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private toastr: ToastrService) {
      this.products = [];
  }

  ngOnInit() {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': this.authGuardService.getLoggedUser().token
      })
    };

    this.httpClient.get(environment.baseUrl + '/product/list', httpOptions)
    .subscribe(
        data => {
          this.products = data['result'];
        },
        error => {
          console.log(error);
        }
    );
  }

  remove(productID: string) {

    this.loadingService.showSreenLoading();

    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': this.authGuardService.getLoggedUser().token
      })
    };

    this.httpClient.delete(environment.baseUrl + '/product/' + productID, httpOptions)
    .subscribe(
        data => {
          this.loadingService.hideSreenLoading();
          this.products = this.products.filter(function(element, index, array) {
            return (data['id'] !== productID);
          });
          this.toastr.success('Produto removido com sucesso.', 'Parabéns');
        },
        error => {
          error.
          this.loadingService.hideSreenLoading();
          this.toastr.success('Produto não removido, tente novamente.', 'Error');
        }
    );
  }

}

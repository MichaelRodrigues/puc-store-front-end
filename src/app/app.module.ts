import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppPageHomeComponent } from './pages/home/page.home.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { BestSellersComponent } from './shared/components/best-sellers/best-sellers.component';
import { BenefitComponent } from './shared/components/benefit/benefit.component';
import { ArrivalsComponent } from './shared/components/arrivals/arrivals.component';
import { SliderComponent } from './shared/components/slider/slider.component';
import { AppRoutingModule } from './app-routing.module';
import { RegisterComponent } from './pages/register/register.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoadingScreenComponent } from './shared/components/loading-screen/loading-screen.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccountComponent } from './pages/account/account.component';
import { BackofficeMenuComponent } from './shared/components/backoffice-menu/backoffice-menu.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductNewComponent } from './pages/product-new/product-new.component';
import { FileUploadModule } from 'ng2-file-upload';
import { CartComponent } from './pages/cart/cart.component';
import { ShoppingComponent } from './pages/shopping/shopping.component';
import { SalesComponent } from './pages/sales/sales.component';
import { SearchComponent } from './pages/search/search.component';
import { UsersComponent } from './pages/users/users.component';

@NgModule({
  declarations: [
    AppPageHomeComponent,
    HeaderComponent,
    FooterComponent,
    BestSellersComponent,
    BenefitComponent,
    ArrivalsComponent,
    SliderComponent,
    RegisterComponent,
    AppComponent,
    LoginComponent,
    LoadingScreenComponent,
    AccountComponent,
    BackofficeMenuComponent,
    ProductsComponent,
    ProductNewComponent,
    CartComponent,
    ShoppingComponent,
    SalesComponent,
    SearchComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FileUploadModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

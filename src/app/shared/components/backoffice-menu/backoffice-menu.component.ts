import { Component, OnInit, Input } from '@angular/core';
import {AuthGuardService} from '../../../shared/services/auth-guard.service';
import { User } from '../../models/User';

@Component({
  selector: 'app-backoffice-menu',
  templateUrl: './backoffice-menu.component.html',
  styleUrls: ['./backoffice-menu.component.css']
})
export class BackofficeMenuComponent implements OnInit {

  loggedUser: User;

  @Input() currentpage: string;

  constructor(private authGuardService: AuthGuardService) {
    this.loggedUser = authGuardService.getLoggedUser();
  }

  ngOnInit() {

  }

}

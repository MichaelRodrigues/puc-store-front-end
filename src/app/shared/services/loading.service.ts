import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(@Inject(DOCUMENT) document) {}

  showSreenLoading() {
    document.getElementById('loading_frame').style.display = 'block';
  }
  hideSreenLoading() {
    document.getElementById('loading_frame').style.display = 'none';
  }

}

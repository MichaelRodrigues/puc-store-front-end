import { Component, OnInit, Inject} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormValidateService} from '../../shared/services/form-validate.service';
import {LoadingService} from '../../shared/services/loading.service';
import { DOCUMENT } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import {FormLoginData} from '../../shared/models/FormLoginData';
import {User} from '../../shared/models/User';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formData: FormLoginData;

  constructor(private httpClient: HttpClient,
    private formValidateService: FormValidateService,
    @Inject(DOCUMENT) document,
    private toastr: ToastrService,
    private router: Router,
    private loadingService: LoadingService,
    private authGuardService: AuthGuardService) {
    this.formData = new FormLoginData();
  }

  ngOnInit() {

  }

  sendForm(profile: string) {
    this.loadingService.showSreenLoading();
    const inputs = document.getElementsByClassName('form_input');

    Array.prototype.forEach.call(inputs, function(el) {
      el.style.borderColor = '#e5e5e5';
    });

    const validation = this.formValidateService.validateLogin(this.formData);

    if (validation.success) {
      this.httpClient.post(environment.baseUrl + '/auth/signin/credentials', this.formData)
      .subscribe(
          data => {
              this.formData = {identifier: '', password: ''};
              this.loadingService.hideSreenLoading();

              this.toastr.success('Login realizado com sucesso.', 'Parabéns', {
                timeOut: 1500
              });

              const pubRouter = this.router;
              const pubAuthGuardService = this.authGuardService;
              setTimeout(function() {
                pubAuthGuardService.setLoggedUser(new User(data));
                pubRouter.navigate(['/account'], {});
              }, 1500);
          },
          error => {
            this.toastr.error('Os dados de login não foram encontrados.', 'Ocorreu um erro', {
              timeOut: 3000
            });

            this.loadingService.hideSreenLoading();
          }
      );
    } else {
      this.loadingService.hideSreenLoading();

      this.toastr.error('Por gentileza, preencha o formulário corretamente.', 'Preenchimento Errado', {
        timeOut: 3000
      });

      const input = document.getElementById('input_' + validation.field);
      input.style.borderColor = '#f00';
    }
  }

}

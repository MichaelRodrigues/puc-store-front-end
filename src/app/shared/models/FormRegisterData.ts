export class FormRegisterData {
  name: string;
  email: string;
  password: string;
  profile: string;
  identifier: string;
}

import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { FormValidateService } from './form-validate.service';

describe('FormValidateService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule]
  }));

  it('should be created', () => {
    const service: FormValidateService = TestBed.get(FormValidateService);
    expect(service).toBeTruthy();
  });
});

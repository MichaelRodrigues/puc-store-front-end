import {FormProductData} from '../../shared/models/FormProductData';

export class FormSaleData {
  products: FormProductData[];

  public constructor(init?: Partial<FormSaleData>) {
    Object.assign(this, init);
  }
}

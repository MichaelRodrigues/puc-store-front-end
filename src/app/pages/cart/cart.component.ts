import { Component, OnInit } from '@angular/core';
import {FormProductData} from '../../shared/models/FormProductData';
import {FormSaleData} from '../../shared/models/FormSaleData';
import {CartService} from '../../shared/services/cart.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { environment } from '../../../environments/environment';
import {LoadingService} from '../../shared/services/loading.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  products: FormProductData[];
  formData: FormSaleData;

  constructor(private cartService: CartService,
    private toastr: ToastrService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private router: Router,
    private authGuardService: AuthGuardService) {

      this.products = this.cartService.getCart();
      this.formData = new FormSaleData();
  }

  ngOnInit() {
  }

  remove(id: string) {
    this.cartService.removeById(id);
    this.products = this.cartService.getCart();

    this.toastr.success('Produto removido do carrinho.', 'Parabéns', {
      timeOut: 500
    });
  }

  endCart() {
    this.formData.products = this.products;

    this.httpClient.post(environment.baseUrl + '/sale', this.formData, {
      headers: new HttpHeaders().set('X-Auth-Token', this.authGuardService.getLoggedUser().token)
    })
    .subscribe(
        data => {
            this.loadingService.hideSreenLoading();
            this.cartService.clean();

            this.toastr.success('Compra finalizada com sucesso..', 'Parabéns', {
              timeOut: 1500
            });

            const pubRouter = this.router;
            setTimeout(function() {
              pubRouter.navigate(['/account'], {});
            }, 1500);
        },
        error => {
          this.toastr.error('Os dados enviados contém erros.', 'Ocorreu um erro', {
            timeOut: 3000
          });

          this.cartService.clean();
          this.loadingService.hideSreenLoading();
        }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AuthGuardService} from '../../../shared/services/auth-guard.service';
import { environment } from '../../../../environments/environment';
import {FormProductData} from '../../../shared/models/FormProductData';
import {LoadingService} from '../../../shared/services/loading.service';
import { ToastrService } from 'ngx-toastr';
import {CartService} from '../../../shared/services/cart.service';

declare var jQuery: any;

@Component({
  selector: 'app-best-sellers',
  templateUrl: './best-sellers.component.html',
  styleUrls: ['./best-sellers.component.css']
})
export class BestSellersComponent implements OnInit {

  bestSellers: FormProductData[];
  env = environment;

  constructor(private authGuardService: AuthGuardService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private toastr: ToastrService,
    private cartService: CartService) {
    this.bestSellers = [];
  }

  ngOnInit() {
    if (this.authGuardService.getLoggedUser()) {
      const httpOptions = {
        headers: new HttpHeaders({
          'X-Auth-Token': this.authGuardService.getLoggedUser().token
        })
      };

      this.httpClient.get(environment.baseUrl + '/product/all', httpOptions)
      .subscribe(
          data => {
            this.bestSellers = data['result'];
          },
          error => {
            console.log(error);
          }
      );
    }
  }

  addToCart(product: FormProductData) {
    this.cartService.addToCart(product);
    this.toastr.success('Produto adicionado ao carrinho.', 'Parabéns', {
      timeOut: 500
    });
  }

}

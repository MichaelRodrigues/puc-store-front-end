import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AuthGuardService} from '../../../shared/services/auth-guard.service';
import { environment } from '../../../../environments/environment';
import {FormProductData} from '../../../shared/models/FormProductData';
import {LoadingService} from '../../../shared/services/loading.service';
import { ToastrService } from 'ngx-toastr';
import {CartService} from '../../../shared/services/cart.service';

declare var jQuery: any;


@Component({
  selector: 'app-arrivals',
  templateUrl: './arrivals.component.html',
  styleUrls: ['./arrivals.component.css']
})
export class ArrivalsComponent implements OnInit {

  arrivals: FormProductData[];
  env = environment;

  constructor(private authGuardService: AuthGuardService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private toastr: ToastrService,
    private cartService: CartService) {
      this.arrivals = [];
    }

  ngOnInit() {
    this.httpClient.get(environment.baseUrl + '/product/all', {})
    .subscribe(
        data => {
          this.arrivals = data['result'];
        },
        error => {
          console.log(error);
        }
    );
  }

  addToCart(product: FormProductData) {
    this.cartService.addToCart(product);
    this.toastr.success('Produto adicionado ao carrinho.', 'Parabéns', {
      timeOut: 500
    });
  }
}

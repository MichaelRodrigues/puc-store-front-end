import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {AuthGuardService} from '../../shared/services/auth-guard.service';
import { environment } from '../../../environments/environment';
import {FormProductData} from '../../shared/models/FormProductData';
import {LoadingService} from '../../shared/services/loading.service';
import { ToastrService } from 'ngx-toastr';
import {User} from '../../shared/models/User';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

  users: FormProductData[];

  constructor(private authGuardService: AuthGuardService,
    private httpClient: HttpClient,
    private loadingService: LoadingService,
    private router: Router,
    private toastr: ToastrService) {
      this.users = [];
      if (!authGuardService.userIsLogged()) {
        this.router.navigate(['/login'], {});
      }
  }

  ngOnInit() {
    this.loadingService.showSreenLoading();

    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': this.authGuardService.getLoggedUser().token
      })
    };

    this.httpClient.get(environment.baseUrl + '/user/list', httpOptions)
    .subscribe(
        data => {
          this.users = data['result'];
          this.loadingService.hideSreenLoading();
        },
        error => {
          console.log(error);
          this.loadingService.hideSreenLoading();
        }
    );
  }

  enable(user: User) {
    this.loadingService.showSreenLoading();

    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': this.authGuardService.getLoggedUser().token
      })
    };

    this.httpClient.get(environment.baseUrl + '/user/enable/' + user.email, httpOptions)
    .subscribe(
        data => {
          user['activated'] = true;
          this.loadingService.hideSreenLoading();
        },
        error => {
          console.log(error);
          this.loadingService.hideSreenLoading();
        }
    );
  }

  disable(user: User) {
    this.loadingService.showSreenLoading();

    const httpOptions = {
      headers: new HttpHeaders({
        'X-Auth-Token': this.authGuardService.getLoggedUser().token
      })
    };

    this.httpClient.get(environment.baseUrl + '/user/enable/' + user.email, httpOptions)
    .subscribe(
        data => {
          user['activated'] = false;
          this.loadingService.hideSreenLoading();
        },
        error => {
          console.log(error);
          this.loadingService.hideSreenLoading();
        }
    );
  }

}

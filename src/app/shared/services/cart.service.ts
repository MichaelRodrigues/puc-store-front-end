import { Injectable } from '@angular/core';
import {FormProductData} from '../../shared/models/FormProductData';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart: FormProductData[];

  constructor() {
    this.cart = this.getCart();
  }

  addToCart(product: FormProductData) {
    let cart = [];
    if (localStorage.getItem('cart')) {
      cart = JSON.parse(localStorage.getItem('cart'));
    }

    cart.push(product);
    localStorage.setItem('cart', JSON.stringify(cart));
    this.cart = cart;
  }

  removeById(id: string) {
    const newCart = this.getCart().filter(function(element, index, array) {
      return (element['id'] !== id);
    });

    localStorage.setItem('cart', JSON.stringify(newCart));
    this.cart = newCart;
  }

  getCart() {
    let cart = [];
    if (localStorage.getItem('cart')) {
      cart = JSON.parse(localStorage.getItem('cart'));
    }

    return cart;
  }

  clean() {
    const cart = [];
    localStorage.setItem('cart', JSON.stringify(cart));
    this.cart = cart;
  }
}
